const express = require('express');
const cookieParser = require('cookie-parser');
const session = require('express-session');
require('dotenv').config();



const app = express();
app.use(cookieParser());

app.use(session({
    secret: process.env.secret,
    resave: false,
    saveUninitialized: false,
    cookie: {
        httpOnly: true,
        maxAge: 3600000
    }
}));

app.get("/", (req, res) => {
    if (!req.session.counter) {
        req.session.counter = 1
        console.log('No session exists...');
        res.redirect('/login');
    } else {
        console.log('Sessions:' + req.session.counter);
        req.session.counter += 1;
        res.redirect('/dashboard');
    }
});
  

app.get('/create', (req, res) => {
    req.session.count = 1;
    res.send('New session was created!');
});

app.get('/login', (req, res) => {
    res.send('Login screen')
})

app.get('/dashboard', (req, res) => {
    res.send('Dashboard')
})


app.listen(5000, () => console.log('App started on Port 5000...'));
